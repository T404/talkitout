
var socket = io.connect('ws://talkitout-lab9.rhcloud.com:8000/');

tarea = document.getElementById('chat');

if(!localStorage.demochatnick){
	var retVal = prompt("Enter your nickname: ", "");
	if (retVal === null || retVal === '') {
        localStorage.demochatnick = 'iDontHaveNick';
    }else{
		localStorage.demochatnick = retVal;
	};
	tarea.value = '\n\nWelcome ' + localStorage.demochatnick + '!\n';
}else{
	tarea.value = '\n\nWelcome back ' + localStorage.demochatnick + '!\n';
}

document.getElementById('mess').onkeypress = function(e){

	e.preventDefault;
	var charCode = (e.which) ? e.which : event.keyCode

	if (charCode == 13) {
		msg_val = $('#mess').val();

		socket.emit('msg',{
			msg_val: msg_val,
			msg_nick: localStorage.demochatnick
		});
		document.getElementById('mess').value = '';
		return false;
	 };
};

$('.link').click(function(){

	var retVal = prompt("Enter your nickname: ", "");
	document.getElementById('mess').value = '';
	if (retVal === null) {
    	localStorage.demochatnick = 'iDontHaveNick';
    }else{
		localStorage.demochatnick = retVal;
	};
	tarea.value += '\n\nName changed to: ' + localStorage.demochatnick + '!\n';
	return false;
});

socket.on('first',function(message){

	$('#num').html(message.num);
});

socket.on('msg',function(message){

	var now = new Date();
	tarea.scrollTop = tarea.scrollHeight;
	tarea.value += '\n' + now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds() + ' - ' + message.msg_nick + ': ' + message.msg_val;
});