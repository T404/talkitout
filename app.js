
var app, express, io, server, ws, room_val, connectCounter=0;

var port = process.env.OPENSHIFT_NODEJS_PORT || 3002;
var ip = process.env.OPENSHIFT_NODEJS_IP || 0;

// Add modules
express = require('express');
app = express();
ws = require('socket.io');

// Configuration
app.use(express["static"]('./static')); // Static files

// Renders main
app.get('/', function(req, res) {

	res.render('index');
});

// Set server
console.log('\n - Welcome to freeTalk!');
server = app.listen(port,ip);
if (ip == 0) ip = 'localhost';
console.log(' - HTTP server started on ' + ip + ', port ' + port + '\n');
io = ws.listen(server);

// Only websockets
io.set('transports', ['websocket']);
console.log(' - Using only websockets\n');

// Socket.IO
io.sockets.on('connection', function(socket){

	connectCounter++;
	// Starting message
	io.sockets.emit('first',{
		num: connectCounter
	});

	// Room message
	socket.on('msg', function(data){

		io.sockets.emit('msg', data);
		//numData: io.sockets.clients(room_val).length
	});

	socket.on('disconnect', function()
	{
		connectCounter--;
		io.sockets.emit('first',{
			num: connectCounter
		});
	});

});